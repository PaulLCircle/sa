$(document).ready(function() {

  if (!Modernizr.svg) {
          $("img").each(function() {
          $(this).attr("src", $(this).data("fallback"));
          });
         }

         

  $("#contact").validate();


/* Fixed Navigation */

    var headerHeight = $('.main-header').outerHeight();
    var navHeight = $('.fixed-nav').outerHeight();

    $(window).on('scroll',function() {
      var windowPosition = $(window).scrollTop();
      if (windowPosition > headerHeight) {
        $('.fixed-nav').addClass('fixed top black');
        $('body').css('margin-top',navHeight);
      } else if (windowPosition <= headerHeight ) {
        $('.fixed-nav').removeClass('fixed black');
        $('body').css('margin-top',0);
      }

    });

  //independent window

  //Accordian

  $(".acc-content").hide();
  $(".acc-btn").click(function(event) {
    event.preventDefault();
  $(this).show().toggleClass("acc-active").next().slideToggle('normal');

  });

  //tabs

  // Show the first tab by default
  $('.tabs-container div').hide();
  $('.tabs-container div:first').show();
  $('.tabs-nav li:first').addClass('tab-active');

  // Change tab class and display content
  $('.tabs-nav a').on('click', function(event){
    event.preventDefault();
    $('.tabs-nav ul li').removeClass('tab-active');
    $(this).parent().addClass('tab-active');
    $('.tabs-container div').hide();
    $($(this).attr('href')).fadeIn(1000);
  });




//anchor animate

  $(function() {
        $('a[href*=#]:not([href=#])').click(function() {
          if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
              $('html,body').animate({
                scrollTop: target.offset().top
              }, 1000);
              return false;
            }
          }
        });
      });

  //parallax
  $window = $(window);
       
  $('section[data-type="background"]').each(function(){
        var $bgobj = $(this); // assigning the object
    
        $(window).scroll(function() {

            var yPos = -($window.scrollTop() / $bgobj.data('speed')); 
            
            // Put together our final background position
            var coords = '50% '+ yPos + 'px';

            // Move the background
            $bgobj.css({ backgroundPosition: coords });
        }); 
    });




  //Navs


  $(".static-menu-btn").click(function(event) {
    event.preventDefault();
  $("nav.main-nav-overlay").fadeIn();

  });

   $(".overlay-close-btn").click(function(event) {
    event.preventDefault();
  $("nav.main-nav-overlay").fadeOut();

  });



  //Pop Ups

  $(".modal").hide();
  $(".modal-bg").hide();

  $(".modal-btn").click(function(event) {
    event.preventDefault();
  $(".modal").fadeIn();
  $(".modal-bg").fadeIn();
  });

  $(".modal-close-btn,.modal-bg").click(function(event) {
    event.preventDefault();
  $(".modal").fadeOut();
  $(".modal-bg").fadeOut();

  });

});